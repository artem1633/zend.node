const fs = require('fs');
const request = require('request');
const option = require('./option');

function get_line(filename, line_no, callback) {
    line_no = parseInt(line_no);
    let data = fs.readFileSync(filename, 'utf8');
    let lines = data.split("\n");
    for (let l in lines) {
        if (l == line_no - 1) {
            callback(null, lines[l].trim());
            return;
        }
    }
    throw new Error('File end reached without finding line');
}

function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}

function validateIpAndPort(inputProxy) {
    let parts = inputProxy.split(":");
    let ip = parts[0].split(".");
    let port = parts[1];
    return validateNum(port, 1, 65535) &&
        ip.length === 4 &&
        ip.every(function (segment) {
            return validateNum(segment, 0, 255);
        });
}

function validateNum(input, min, max) {
    let num = +input;
    return num >= min && num <= max && input === num.toString();
}

async function apiRequestOut(method, link, body) {

    if (method === 'post') {
        await request.post(link).form(body)
    }
    if (method === 'get') {
        await request.get(link)
    }
}

function apiRequestIn() {

    return new Promise(function (resolve, reject) {
        request(option.url, function (error, res, body) {
            if (!error && res.statusCode === 200) {
                // console.log(body);
                resolve(body);
            } else {
                reject(error);
            }
        });
    });
}

function onlyNumber(str) {
    return Number(str.replace(/\D+/g, ""))
}

function regClean(str) {

    let result = str.match(/(\d+),(\d+)/);
    if (result !== null) {
        return result[1] + result[2] + '00';
    } else {
        result = str.match(/(\d+) т/);
        if (result !== null) {
            return parseInt(result[1] + '000')
        } else {
            result = onlyNumber(str);
            // console.log('three', result);
            if (!isNaN(result)) {
                return result
            } else {
                return 0;
            }
        }
    }
}

function regCleanTime(str) {

    let result = str.match(/(\d+),(\d+)/);
    if (result !== null) {
        return ((result[1] * 60) + 30);
    } else {
        result = str.match(/(\d+) с/);
        if (result !== null) {
            return parseInt(result[1]);
        } else {
            result = str.match(/(\d+) м/);
            // console.log('three', result);
            if (result !== null) {
                return result[1] * 60;
            } else {
                return 0;
            }
        }
    }
}

async function parseData(page, url, data) {
    console.log('page pars proces1');
    try {
        await page.goto(url, {
            waitUntil: 'domcontentloaded',
            timeout: 6000
        });
    } catch (e) {
        // return {
        //     result: false,
        //     comment: 'failed load page'
        // }
    }
    await page.waitFor(3000);
    console.log(url);
    console.log('page pars proces2');
    await page.screenshot({path: 'evaluate.png'});
    let parsResult;

    try {
        parsResult = await page.evaluate(async function (data) {

            let link = '',
                type = 0,
                title = '',
                date = 0,
                view_count = 0,
                reading_prosent = 0,
                reading_count = 0,
                average_time = 0,
                like_count = 0,
                comment_count = 0,
                project_id = data.project_id,
                channel_id = data.channel_id;

            try {
                link = await location.href;
            } catch (e) {

            }
            try {
                type = await document.getElementsByTagName('article').length >= 1 ? 2 : 1;
            } catch (e) {

            }
            try {
                title = await document.getElementsByName('twitter:image:alt')[0].content;
            } catch (e) {

            }
            try {
                date = await document.getElementsByClassName('article-stat__date')[0].textContent;
            } catch (e) {

            }
            try {
                view_count = await document.getElementsByClassName('article-stat__count')[0].innerHTML;
            } catch (e) {

            }
            try {
                reading_count = await document.getElementsByClassName('article-stat__count')[1].innerHTML;
            } catch (e) {

            }
            try {
                reading_prosent = await document.getElementsByClassName('article-stat-tip__value')[1].innerText;
            } catch (e) {

            }
            try {
                average_time = await document.getElementsByClassName('article-stat__count')[2].innerHTML;
            } catch (e) {

            }
            try {
                like_count = await document.getElementsByClassName('likes-count__count')[0].innerText;
            } catch (e) {

            }
            try {
                comment_count = await document.getElementsByClassName('comments-count__count')[0].innerText;
            } catch (e) {
                try {
                    comment_count = await document.getElementsByClassName('comments-container-header _count')[0].innerText;
                } catch (e) {

                }
            }


            return await {
                "link": link,
                "type": type,
                "title": title,
                "date": date,
                "view_count": view_count,
                "reading_count": reading_count,
                "reading_prosent": reading_prosent,
                "average_time": average_time,
                "like_count": like_count,
                "comment_count": comment_count,
                "project_id": project_id,
                "channel_id": channel_id
            };

        }, data);
    } catch (e) {
        console.log(e);
        await page.screenshot({path: 'evaluatefailed.png'});

    }

    // console.log('WHIYTE: ',parsResult);

    // try {
    //     let proc = await parsResult.reading_prosent.split(',');
    //
    //     if (await proc[1].split('').indexOf('%') !== -1) {
    //         parsResult.reading_prosent = proc[1];
    //     } else {
    //         parsResult.reading_prosent = proc[2];
    //     }
    // } catch (e) {
    //     // console.log(e);
    //     parsResult.reading_prosent = 0;
    // }

    try {

    } catch (e) {

    }

    if ((parsResult.link.search(/https:\/\/zen.yandex.ru/)) === -1) {
        return false;
    }


    // let view_countReg = ;

    try {
        parsResult.view_count = regClean(parsResult.view_count);
    } catch (e) {
    }
    try {
        parsResult.reading_count = regClean(parsResult.reading_count);
    } catch (e) {
    }
    try {
        parsResult.average_time = regCleanTime(parsResult.average_time)
    } catch (e) {

    }
    try {
        if (parsResult.like_count !== 0) {
            let like_countF = parseInt(parsResult.like_count.replace(/\D+/g, ""), 10);
            if (!isNaN(like_countF)) {
                parsResult.like_count = like_countF;
            } else {
                parsResult.like_count = 0
            }
        }

    } catch {
    }
    try {
        if (parsResult.comment_count !== 0) {
            let comment_countF = parseInt(parsResult.comment_count.replace(/\D+/g, ""), 10);
            console.log('comment_countF:', comment_countF);
            if (!isNaN(comment_countF)) {
                parsResult.comment_count = comment_countF;
            } else {
                parsResult.comment_count = 0
            }

        }
    } catch (e) {

    }
    try {

        parsResult.reading_prosent = Math.ceil(parsResult.reading_count / parsResult.view_count * 100)
        // if (parsResult.reading_prosent !== 0) {
        //     let reading_prosentF = parseInt(parsResult.reading_prosent.replace(/\D+/g, ""), 10);
        //     if (!isNaN(reading_prosentF)) {
        //         parsResult.reading_prosent = reading_prosentF;
        //     } else {
        //         parsResult.reading_prosent = 0
        //     }
        //
        // }
    } catch (e) {

    }
    return parsResult;
    // console.log('CHANGED: ', parsResult);


}

module.exports = {
    get_line,
    randomInteger,
    apiRequestOut,
    validateIpAndPort,
    apiRequestIn,
    parseData,
};