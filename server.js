const express = require('express');
const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const script = require('./functionAll');
const parser = require('./parser');
const os = require('os');
const exec = require('executive');
const proxyChain = require('proxy-chain');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// мониторит работу процесора и перезапускает если средняя нагрузка на проц меньше 1% в 5 минут
// setInterval(function () {
//     let proces = os.loadavg();
//     console.log('proc:', proces);
//     if(proces[1] < 0.9 ){
//         exec('pm2 restart 0');
//     }
//
// }, 300000);

// флаг запрещающий бесконечный запуск
global.try = false;

// let arr = [
//     // 'https://www.instagram.com/p/BzbA9VWi9s3/',
//     'https://zen.yandex.ru/siiir'
// ];
//
// let proxy = [
//     '95.167.177.18:8080'
// ];
// parser.index(arr, proxy);


app.get('/start', async function (req, res) {

    res.status(200).send(true);

    if (global.try !== true) {

        // запрос данных
        let obj = await script.apiRequestIn();
        obj = await JSON.parse(obj);

        console.log(obj);

        // запуск осн. функции
        await parser.index(
            obj,
        );

    }


});

// стук каждые 15с на наявность миссий
// setInterval(async function () {
//     if (global.try !== true) {
//
//         // запрос данных
//         let obj = await script.apiRequestIn();
//         obj = await JSON.parse(obj);
//
//         console.log(obj);
//
//         // проверки на пустоту приходящих данных
//
//         // if (obj[0].channel_id[0] === undefined) {
//         //     await script.apiRequestOut('post', 'https://like.teo-send.ru/api/like/mission-instagram', JSON.stringify({error: obj.link + ' - данные не пришли!'}));
//         // } else {
//
//         // запуск осн. функции
//         await parser.index(
//             obj,
//         );
//         // }
//     }
//
// }, 86400000);

// запуск сервера
http.listen(3003, function () {
    console.log(`app listening on port 3003!`);
});


